resource "aws_eip" "nat_ip" {
  vpc = true
  tags = {
    Name = "nat_eip"
  }
}

resource "aws_nat_gateway" "nat_gateway" {
  subnet_id = aws_subnet.public_subnet.id
  connectivity_type = "public"
  allocation_id = aws_eip.nat_ip.id
  depends_on = [aws_internet_gateway.internet_gateway, aws_eip.nat_ip]
}
