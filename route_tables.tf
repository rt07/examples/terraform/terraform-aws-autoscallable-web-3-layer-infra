# https://stackoverflow.com/a/59965409/4814427

#Route Tables

resource "aws_route_table" "public_route" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }
}

resource "aws_route_table" "private_route" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gateway.id
  }
}

#Route Table Associations

resource "aws_route_table_association" "public_subnet_route_association" {
  route_table_id = aws_route_table.public_route.id
  subnet_id = aws_subnet.public_subnet.id
}

resource "aws_route_table_association" "private_subnet_route_table_association" {
  route_table_id = aws_route_table.private_route.id
  subnet_id = aws_subnet.private_subnet.id
}

resource "aws_route_table_association" "db_subnet_route_table_association" {
  route_table_id = aws_route_table.private_route.id
  subnet_id = aws_subnet.db_subnet.id
}

resource "aws_route_table_association" "spare_subnet_route_table_association" {
  route_table_id = aws_route_table.private_route.id
  subnet_id = aws_subnet.spare_subnet.id
}