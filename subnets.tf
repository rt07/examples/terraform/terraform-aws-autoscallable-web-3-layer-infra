resource "aws_subnet" "public_subnet" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.0.0/24"
  map_public_ip_on_launch = true
  tags = {
    name = "public_subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.1.0/25"

  tags = {
    name = "private_subnet"
  }
}

resource "aws_subnet" "db_subnet" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.1.128/26"

  tags = {
    name = "db_subnet"
  }
}

resource "aws_subnet" "spare_subnet" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.0.1.192/26"

  tags = {
    name = "spare_subnet"
  }
}