resource "aws_instance" "ec2_web_server" {
  subnet_id = aws_subnet.public_subnet.id
  ami = "ami-052f483c20fa1351a"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.bravo.id]
  key_name = "Rafaf-Ubuntu"
  user_data = "${file("scripts/install_web_server.sh")}"
  tags = {
    name = "ec2_web_server"
  }
}
