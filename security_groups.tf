resource "aws_security_group" "alfa" {
  name = "alfa"
  description = "Allow HTTP and bravo"
  vpc_id = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "alfa_http_rule" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.alfa.id
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_security_group_rule" "alfa_egress_rule" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.alfa.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_security_group_rule" "alfa_allow_bravo_rule" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.alfa.id
  source_security_group_id = aws_security_group.bravo.id
  to_port           = 0
  type              = "ingress"
}



resource "aws_security_group" "bravo" {
  name = "bravo"
  description = "Allow HTTP SSH and Alfa"
  vpc_id = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "bravo_http_rule" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.bravo.id
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_security_group_rule" "bravo_ssh_rule" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.bravo.id
  to_port           = 22
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_security_group_rule" "bravo_allow_all_from_alfa" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.bravo.id
  source_security_group_id = aws_security_group.alfa.id
  to_port           = 0
  type              = "ingress"
}

resource "aws_security_group_rule" "bravo_egress_rule" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.bravo.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}


resource "aws_security_group" "delta" {
  name = "delta"
  description = "allow all"
  vpc_id = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "delta_ingress_allow_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.delta.id
  to_port           = 0
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}

resource "aws_security_group_rule" "delta_egress_allow_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.delta.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
}